﻿using System;
using System.IO;
using System.Collections.Generic;

// test comment
// integration with BitBucket
namespace Pairs
{
    class Solution
    {
        private static int _k = 2;
        private static int _arraySize = 5;
        private static HashSet<Int32> _nums = new HashSet<Int32>();

        // this is for taking input from stdin that is redirected
        // from a text file. 
        // this is what will get used with the hackerrank submission
        private static void readInput()
        {
            //Console.WriteLine("Taking input from stdin ...");
            string[] lineArgs = Console.ReadLine().Split(' ');
            // number of elements in the problem array
            _arraySize = int.Parse(lineArgs[0]);

            // K value - the value we'll use in checking for our pair per 
            // problem description
            _k = int.Parse(lineArgs[1]);

            // get the array we'll be analyzing
            lineArgs = Console.ReadLine().Split(' ');
            for (int ii = 0; ii < lineArgs.Length; ii++)
            {
                _nums.Add(int.Parse(lineArgs[ii]));
            }
        }

        // this is what we use if we're reading input from a file whose file
        // name is supplied as a command line argument. it's just an 
        // opportunity to practice with input streams.
        private static void readInput(string fname)
        {
            FileStream inStream = new FileStream(fname, FileMode.Open);
            StreamReader reader = new StreamReader(inStream);

            string[] lineArgs = reader.ReadLine().Split(' ');
            // number of elements in the problem array
            _arraySize = int.Parse(lineArgs[0]);

            // K value - the value we'll use in checking for our pair 
            // per problem description
            _k = int.Parse(lineArgs[1]);

            // get the array we'll be analyzing
            lineArgs = reader.ReadLine().Split(' ');
            for (int ii = 0; ii < lineArgs.Length; ii++)
            {
                _nums.Add(int.Parse(lineArgs[ii]));
            }
        }

        private static int calcPairCount()
        {
            int res = 0;
            foreach (int ii in _nums)
            {
                if (_nums.Contains(ii - _k))
                {
                    res++;
                }
                if (_nums.Contains(ii + _k))
                {
                    res++;
                }
            }
            return res / 2;
        }

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                readInput(args[0]);
            }
            else
            {
                readInput();
            }
            Console.WriteLine(calcPairCount());
        }
    }
}
