﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace MissingNumbers
{
    // Missing Numbers hackerrank problem
    // Area: algorithms -> search
    // Difficulty: Easy
    // Submissions: 26481 
    // Success rate: 83.47%

    class Program
    {
        private static Dictionary<int, int> _numListA = new Dictionary<int,int>();
        private static int[] _numListB; 

        private static void getProblemInputVals(StreamReader reader) 
        {
            //Console.WriteLine("In common strIn routine!");

            // this is the number of elements in the 'A' list. 
            // however, we don't need this number. we'll just gobble this input
            // up and continue on.
            reader.ReadLine();

            foreach (string num in reader.ReadLine().Split(' '))
            {
                //Console.Write(num + " ");
                int numVal = int.Parse(num);
                int numCount = 0;
                if (_numListA.TryGetValue(numVal, out numCount))
                    _numListA[numVal]++;
                else
                    _numListA.Add(numVal, 1);
            }
            // number of elements in the 'B' list.
            // we don't need this guy but we have to read it.
            reader.ReadLine();

            // we don't need a fancy collection for list B. it's 
            // just a reference that we'll cycle through later. order doesn't
            // matter.
            _numListB = reader.ReadLine().Split(' ').Select(s => int.Parse(s)).ToArray();
        }

        // this is for taking input from stdin that is redirected
        // from a text file. 
        // this is what will get used with the hackerrank submission
        private static void readStdInput()
        {
            //Console.WriteLine("Taking input from stdin ...");
            if (Console.IsInputRedirected)
            {
                Stream inputStream = Console.OpenStandardInput();
                StreamReader reader = new StreamReader(inputStream);
                getProblemInputVals(reader);
            }
        }

        // this is what we use if we're reading input from a file whose file
        // name is supplied as a command line argument. it's just an 
        // opportunity to practice with input streams.
        private static void readFileInput(string fname)
        {
            Console.WriteLine("Taking input from file " + fname + " ...");
            FileStream inStream = new FileStream(fname, FileMode.Open);
            StreamReader reader = new StreamReader(inStream);

            getProblemInputVals(reader);
        }

        static void reportMissingNumbers()
        {
            foreach (int v in _numListB)
                if (_numListA.ContainsKey(v))
                    _numListA[v]--;

            IEnumerable<int> res = _numListA
                                        .Where(entry => entry.Value != 0)
                                        .OrderBy(entry => entry.Key)
                                        .Select(entry => entry.Key);

            foreach (int var in res) {
                Console.Write(var + " ");
            }
            Console.WriteLine("");
        }

        static void Main(string[] args)
        {
            if (args.Length > 0)
                readFileInput(args[0]);
            else
                readStdInput();
            reportMissingNumbers();            
        }
    }
}
