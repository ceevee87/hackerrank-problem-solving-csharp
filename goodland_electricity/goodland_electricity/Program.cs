﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace GoodlandElectricity
{
    class Program
    {
        private static int _numCities;
        private static int _towerRadius;
        private static int[] _towerLocations;

        private static void getProblemInputVals(StreamReader reader)
        {
            //Console.WriteLine("In common strIn routine!");

            int[] intsLineIn = reader.ReadLine().Split(' ').Select(s => int.Parse(s)).ToArray();
            if (intsLineIn.Length > 1)
            {
                _numCities = intsLineIn[0];
                _towerRadius = intsLineIn[1];
            }

            // this is an array that designates for each city how many
            // electrical towers they have. If there is a zero value then
            // there are no tower locations.
            _towerLocations = reader.ReadLine().Split(' ').Select(s => int.Parse(s)).ToArray();
        }

        // this is for taking input from stdin that is redirected
        // from a text file. 
        // this is what will get used with the hackerrank submission
        private static void readStdInput()
        {
            //Console.WriteLine("Taking input from stdin ...");
            if (Console.IsInputRedirected)
            {
                Stream inputStream = Console.OpenStandardInput();
                StreamReader reader = new StreamReader(inputStream);
                getProblemInputVals(reader);
            }
        }

        // this is what we use if we're reading input from a file whose file
        // name is supplied as a command line argument. it's just an 
        // opportunity to practice with input streams.
        private static void readFileInput(string fname)
        {
            Console.WriteLine("Taking input from file " + fname + " ...");
            FileStream inStream = new FileStream(fname, FileMode.Open);
            StreamReader reader = new StreamReader(inStream);

            getProblemInputVals(reader);
        }

        private static List<int> getEnabledTowers()
        {
            var res = new List<int>();
            var firstTower = -1;

            // find the first tower that we need to turn on.
            for (int ii = 0; ii < _towerRadius; ii++)
            {
                // check if the current city has an electrical tower
                if (_towerLocations[ii] > 0)
                    firstTower = ii;
            }

            // we were unable to find a city with an electrical tower
            // within _towerRadius distance from the origin.
            if (firstTower == -1)
                return null;
            res.Add(firstTower);

            // let's find the last tower. this will be similar to what we did 
            // with the first tower, we'll just be working from the end of the 
            // array backwards
            int lastTower = -1;
            for (int ii = _numCities-1; ii >= _numCities-_towerRadius; ii--)
            {
                // check if the current city has an electrical tower
                if (_towerLocations[ii] > 0)
                    lastTower = ii;
            }

            // we were unable to find a city with an electrical tower
            // within _towerRadius distance from the end.
            if (lastTower == -1)
                return null;

            // for the algorithm to proceed we need to have distinct first and
            // last towers such that the last tower follows the first tower.
            if (lastTower <= firstTower)
                return res;
            res.Add(lastTower);

            bool done = false;
            bool fail = false;
            var prevTower = firstTower;
            while (!done)
            {
                // curTower is an offset relative to the previous tower that
                // we activated (turned on).
                int curTower = 0;
                int ii = 0;
                for (ii = 0; ii < 2 * _towerRadius; ii++)
                {
                    if (ii + prevTower == lastTower) 
                        return res;
                    else if (_towerLocations[ii + prevTower] != 0)
                        curTower = ii;
                }
                // if we couldn't find a tower to turn on within the range
                // of 2*_towerRadius then curTower will be stuck at 0.
                // we can stop then
                if (curTower > 0)
                {
                    prevTower += curTower;
                    res.Add(prevTower);
                }
                else
                {
                    done = true;
                    fail = true;
                }
            }
            if (res.Count > 0 && !fail)
                return res;
            return null;
        }

        static void Main(string[] args)
        {
            //Console.WriteLine("Goodland Electricity Hacker Rank problem.");
            if (args.Length > 0)
                readFileInput(args[0]);
            else
                readStdInput();

            var towers = getEnabledTowers();
            if (towers == null)
                Console.WriteLine("-1");
            else
                Console.WriteLine(towers.Count);
        }
    }
}
